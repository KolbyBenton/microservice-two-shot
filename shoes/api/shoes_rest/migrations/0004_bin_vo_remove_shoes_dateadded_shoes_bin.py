# Generated by Django 4.0.3 on 2023-06-02 22:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0003_shoes_picture_url'),
    ]

    operations = [
        migrations.CreateModel(
            name='Bin_VO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('import_href', models.CharField(max_length=200)),
                ('wardrobe_name', models.CharField(max_length=200)),
            ],
        ),
        migrations.RemoveField(
            model_name='shoes',
            name='DateAdded',
        ),
        migrations.AddField(
            model_name='shoes',
            name='Bin',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Bin', to='shoes_rest.bin_vo'),
        ),
    ]
