from .models import Shoes
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder

# Create your views here.
class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "Manufacturer",
        "Model",
        "Color",
        "Picture_URL",
    ]

@require_http_methods(["GET", "POST", "DELETE"])
def Shoes_List(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": list(shoes)},
            encoder=ShoeDetailEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        Shoes.objects.all().delete()
        return JsonResponse({"message": "Succesfully Deleted"})
