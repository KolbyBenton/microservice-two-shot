from django.db import models

# Create your models here.

class Bin_VO(models.Model):
    import_href = models.CharField(max_length=200)
    wardrobe_name = models.CharField(max_length=200)

class Shoes(models.Model):
    Manufacturer = models.CharField(max_length=200)
    Model = models.CharField(max_length=200)
    Color = models.CharField(max_length=200)
    Picture_URL = models.URLField(null=True)

    Bin = models.ForeignKey(
        Bin_VO,
        related_name="Bin",
        on_delete=models.CASCADE,
        null=True
    )


    def __str__(self):
        return self.Manufacturer
