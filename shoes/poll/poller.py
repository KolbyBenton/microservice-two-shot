import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from hats_rest, here.
from shoes_rest.models import Bin_VO

def get_wardrobe ():
    response = requests.get("http://wardrobe-api:8000/api/locations/")
    content = json.loads(response.content)
    for bin in content ["bins"]:
        Bin_VO.objects.update_or_create(
            import_href=bin["href"],
            defaults={"wardrobe_name": bin["wardrobe_name"]},
        )


def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            get_wardrobe()
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
