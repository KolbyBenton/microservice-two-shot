import React, { useEffect, useState } from 'react';

const ShoesList = () => {
  const [shoes, setShoes] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/Shoes/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
    }
  }



  useEffect(() => {
    fetchData();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Manufacturer</th>
          <th>Model</th>
          <th>Color</th>
          <th>Picture</th>
          <th>Bin Location</th>
        </tr>
      </thead>
      <tbody>
        {shoes.map(shoe => (
          <tr key={shoe.href}>
            <td>{shoe.Manufacturer}</td>
            <td>{shoe.Model}</td>
            <td>{shoe.Color}</td>
            <td>{shoe.Picture_URL}</td>
            <td>{shoe.Bin}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};
export default ShoesList;
