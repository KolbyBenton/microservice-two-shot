from django.urls import path
from .views import (
    hat_list,

)

# app_name = 'hats_rest'

urlpatterns = [
    path('hats/', hat_list, name='hat_list'),
]
