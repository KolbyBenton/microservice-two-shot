from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Hat, LocationVO
import json
from common.json import ModelEncoder

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["name", "import_href"]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["name"]

    def get_extra_data(self, o):
        return {"location": o.location.name}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name",
        "fabric",
        "color",
        "picture",
        "location",
    ]
    encoders = {
        "location": LocationVO(),
    }

@require_http_methods(["GET", "POST", "DELETE"])
def hat_list(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatDetailEncoder,
        )

    elif request.method == 'POST':
        content = json.loads(request.body)

        try:
            location_href = content["location"]["import_href"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location href"},
                status=404,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )

    elif request.method == "DELETE":
        Hat.objects.all().delete()
        return JsonResponse({"message": "yo, it deleted"})
