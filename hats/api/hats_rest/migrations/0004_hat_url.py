# Generated by Django 4.0.3 on 2023-06-02 21:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0003_hat_color'),
    ]

    operations = [
        migrations.AddField(
            model_name='hat',
            name='URL',
            field=models.URLField(null=True),
        ),
    ]
