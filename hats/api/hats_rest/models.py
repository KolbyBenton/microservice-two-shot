from django.db import models


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet = models.CharField(max_length=200)

class Hat(models.Model):
    name = models.CharField(max_length=200)
    fabric = models.CharField(max_length=200, null=True)
    color = models.CharField(max_length=200, null=True)
    picture = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return self.name
